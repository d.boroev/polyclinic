<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int $doctor_name
 * @property int $patient_name
 * @property string $date
 * @property string $time
 * @property string $comment
 *
 * @property Doctor $doctor
 * @property Patient $patient
 * @property Visit[] $visits
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_name', 'patient_name', 'date', 'time'], 'required'],
            [['doctor_name', 'patient_name'], 'default', 'value' => null],
            [['doctor_name', 'patient_name'], 'string'],
            [['date', 'time'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['doctor_name'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_name' => 'name']],
            [['patient_name'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doctor_name' => 'ФИО доктора',
            'patient_name' => 'ФИО пациента',
            'date' => 'Дата',
            'time' => 'Время',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['name' => 'doctor_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['name' => 'patient_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisits()
    {
        return $this->hasMany(Visit::className(), ['request_id' => 'id']);
    }
}
