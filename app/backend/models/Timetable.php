<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "timetable".
 *
 * @property int $id
 * @property string doctor_name
 * @property string $date
 * @property string $start
 * @property string $end
 *
 * @property Doctor $doctor
 */
class Timetable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timetable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_name', 'date', 'start', 'end'], 'required'],
            [['doctor_name'], 'string'],
            [['date', 'start', 'end'], 'safe'],
            [['doctor_name'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doctor_name' => 'ФИО доктора',
            'date' => 'Дата',
            'start' => 'Начало',
            'end' => 'Конец',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['name' => 'doctor_name']);
    }
}
