<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'doctor_name')->textInput() ?>

    <?= $form->field($model, 'patient_name')->textInput() ?>

    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::class, [
            'pluginOptions' => [
                'format' => 'yyyy-m-d',
            ]
    ]) ?>

    <?= $form->field($model, 'time')->widget(\kartik\time\TimePicker::class, [
        'pluginOptions' => [
            'showSeconds' => true,
            'showMeridian' => false,
            'minuteStep' => 1,
            'secondStep' => 5,
        ]
    ]) ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
