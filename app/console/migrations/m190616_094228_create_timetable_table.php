<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%timetable}}`.
 */
class m190616_094228_create_timetable_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%timetable}}', [
            'id' => $this->primaryKey(),
            'doctor_name' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'start' => $this->time()->notNull(),
            'end' => $this->time()->notNull(),
        ]);

        $this->addForeignKey('timetable_doctor_name_pk', '{{%timetable}}', 'doctor_name', '{{%doctor}}', 'name', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%timetable}}');
    }
}
