<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%request}}`.
 */
class m190616_094320_create_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%request}}', [
            'id' => $this->primaryKey(),
            'doctor_name' => $this->string()->notNull(),
            'patient_name' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'comment' => $this->string(),
        ]);

        $this->addForeignKey('request_doctor_doctor_name_pk', '{{%request}}', 'doctor_name', '{{%doctor}}', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('request_patient_patient_name_pk', '{{%request}}', 'patient_name', '{{%patient}}', 'name', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%request}}');
    }
}
