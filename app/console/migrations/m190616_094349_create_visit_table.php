<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%visit}}`.
 */
class m190616_094349_create_visit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%visit}}', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
            'description' => $this->string(),
            'price' => $this->decimal()->notNull(),
        ]);

        $this->addForeignKey('visit_request_request_id_pk', '{{%visit}}', 'request_id', '{{%request}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('visit_service_service_id_pk', '{{%visit}}', 'service_id', '{{%service}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%visit}}');
    }
}
